#!/bin/sh

###################################
# Global settings (do not change) #
###################################

# set targeted user HOME path
HOME="/home/$(logname)"
# colors
green="\e[0;92m"
red="\e[0;91m"
reset="\e[0m"
# errors count
ERRORS=0
# Log file
LOGFILE="install.log"
# empty log file
echo > $LOGFILE

#####################
# Utility functions #
#####################

custom_echo() {
    echo -en "* $1"
}
ok() {
    if [ ! $? -eq 0 ]; then
        echo -e "...${red}KO${reset}"
        let "ERRORS++"
    else
        echo -e "...${green}OK${reset}"        
    fi
}
ko() {
    if [ $? -eq 1 ]; then
        echo -e "...${red}KO${reset}"
        echo "${red}A necessary step failed. Exiting...${reset}"
        let "ERRORS++"
        finish
    else
        echo -e "...${green}OK${reset}"        
    fi
}
create () {
    [ ! -d "$1" ] && [ ! -f "$1" ] && mkdir -p "$1"
}
move() {
    if [ -d "$2" ] || [ -f "$2" ]; then
    read -e -p "WARNING: $2 exist. Replace it ? (y/n) " go
    if [ "$go" != "y" ]; then
    return 2 
    fi
    fi
    custom_echo "moving $1 to $2"
    # create the path if necessary
    create "$2"
    # remove the folder/file if exist
    sudo rm -rf "$2"
    # copy the folder/file
    cp -r "./$1" "$2" >> $LOGFILE 2>&1
    ok
}
move_sudo() {
    if [ -d "$2" ] || [ -f "$2" ]; then
    read -e -p "WARNING: $2 exist. Replace it ? (y/n) " go
    if [ "$go" != "y" ]; then
    return 2 
    fi
    fi
    custom_echo "moving $1 to $2"
    # create the path if necessary
    [ ! -d "$1" ] && [ ! -f "$1" ] && sudo mkdir -p "$1"
    # remove the folder/file if exist
    sudo rm -rf "$2"
    # copy the folder/file
    sudo cp -r "./$1" "$2" >> $LOGFILE 2>&1
    ok
}   
setup() {
    custom_echo "setting up $1 to $2 for all services"
    find "$DATA_PATH/admin/serv" -type "f" -exec sed -i "s+$1+$2+g" {} \;
    ok
}
link() {
    custom_echo "creating symbolic link to $1 as $2"
    create "$1"
    rm -rf "$2"
    ln -sf "$1" "$2" >> $LOGFILE 2>&1
    ok
}
run() {
    custom_echo "$@"
    eval $@ >> $LOGFILE 2>&1
    ok
}
run_nofail() {
    custom_echo "$@"
    eval $@ >> $LOGFILE 2>&1
    ko
}
add() {
    custom_echo "adding Midas app $1"
    midas app add admin $1 >> $LOGFILE 2>&1
    ok
}
finish() {
    echo -e "\nInstall finished !\n"
    if [ "$ERRORS" != "0" ]; then
    echo -e "${red}$ERRORS${reset} errors catched. See install.log for details.\n"
    fi
    read -p "Do you want to start Midas now ? (y/n): " go
    if [ "$go" == "y" ]; then
    midas service start admin all 
    fi
    exit    
}

#######################
# Starting the script #
#######################
echo -e "
Welcome to the Midas installation script !

What do you want to do :

1. Install a local instance for developpement
2. Install a server instance on an Alpine Linux environnement

"
read -p "Answer: " part

#################
# Local Install #
#################
if [ "$part" == "1" ]; then

BASE_URL="midas"
echo "Base URL: midas"
DOMAIN="localhost"
echo "Domain: localhost"
read -p "Admin password (default 'admin'): " ADMIN_PSWD
ADMIN_PSWD=${ADMIN_PSWD:-admin}
read -p "File editor (default 'vi'): " EDITOR
EDITOR=${EDITOR:-vi}
read -p "Command path (default '/usr/local/bin'): " BIN_PATH
BIN_PATH=${BIN_PATH:-/usr/local/bin}
read -p "Midas path (default '~/.local/share/Midas'): " DATA_PATH
DATA_PATH=${DATA_PATH:-$HOME/.local/share/Midas}

# install the script to the right location
move_sudo midas $BIN_PATH/midas
# setting up midas
sudo sed -i "s+LOCAL=+LOCAL=1+" $BIN_PATH/midas
sudo sed -i "s+EDITOR=+EDITOR=$EDITOR+" $BIN_PATH/midas
sudo sed -i "s+^MIDAS_DIR=+MIDAS_DIR=$DATA_PATH/user+" $BIN_PATH/midas
sudo sed -i "s+^ADMIN_MIDAS_DIR=+ADMIN_MIDAS_DIR=$DATA_PATH/admin+" $BIN_PATH/midas
# giving permission to execute midas
sudo chmod +x $BIN_PATH/midas
# adding admin apps
add caddy
add lighttpd
add openldap
# adding admin services
move admin_services/caddy.main $DATA_PATH/admin/serv/caddy.main
move admin_services/lighttpd.main $DATA_PATH/admin/serv/lighttpd.main
move admin_services/openldap.main $DATA_PATH/admin/serv/openldap.main
# setting up admin services
setup xxx $BASE_URL
setup xx $DOMAIN
setup admin_pswd $ADMIN_PSWD

##################
# Server Install #
##################
elif [ "$part" == "2" ]; then
echo "Not implemented yet"

# NOT YET IMPLEMENTED


##########
# Finish #
##########
else
exit
fi
finish
exit