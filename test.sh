#!/bin/sh

###################################
# Global settings (do not change) #
###################################

# this script directory absolute path
SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
# colors
green="\e[0;92m"
red="\e[0;91m"
reset="\e[0m"
# errors count
ERRORS=0
# Log file
LOGFILE="test.log"
# empty log file
echo > $LOGFILE

#####################
# Utility functions #
#####################

ok() {
    if [ ! $? -eq 0 ]; then
        echo -e "...${red}KO${reset}"
        let "ERRORS++"
    else
        echo -e "...${green}OK${reset}"        
    fi
}
ko() {
    if [ $? -eq 1 ]; then
        echo -e "...${red}KO${reset}"
        echo "${red}A necessary step failed. Exiting...${reset}"
        let "ERRORS++"
        finish
    else
        echo -e "...${green}OK${reset}"        
    fi
}
create_if_not_exist() {
    set +e
    [ ! -d $1 ] && mkdir -p $1
    set -e
}
dir_exist() {
    [ -d $1 ] && return 0
    return 1
}
file_exist() {
    [ -f $1 ] && return 0
    return 1  
}
container_running() {
    r=$(docker ps | grep Up | grep $1 | wc -l)
    [ $r -eq 0 ] && return 1
    return 0
}
error() {
    echo "Error: $@"
    exit 1
}
# display title
T() {
    echo -e "\n$1\n"
}
# display test description
e() {
    echo -en "* $1"
}
# run test command
t() {
    eval $@ >> $LOGFILE 2>&1
    ok
}

###############
# PREPARE ENV #
###############

# Create temporary folder
rm -rf env_test
mkdir env_test
# Copy and setup midas
cp midas env_test/midas
sed -i "s+^LOCAL=+LOCAL=1+" env_test/midas
sed -i "s+^EDITOR=+EDITOR=cat+" env_test/midas
sed -i "s+^MIDAS_DIR=+MIDAS_DIR=$SCRIPT_DIR/env_test/user+" env_test/midas
sed -i "s+^ADMIN_MIDAS_DIR=+ADMIN_MIDAS_DIR=$SCRIPT_DIR/env_test/admin+" env_test/midas

# enter the test folder
cd env_test

#########
# TESTS #
#########

# test app add
T "test app add"

e "add app lighttpd"
t ./midas app add lighttpd
e "directory /user/app/lighttpd exist"
t dir_exist user/app/lighttpd
e "add admin app caddy"
t ./midas app add admin caddy
e "directory /user/app/caddy exist"
t dir_exist admin/app/caddy

# test service add
T "service add"

e "add service lighttpd.test"
t ./midas service add lighttpd test
e "directory /user/serv/lighttpd.test exist"
t dir_exist user/serv/lighttpd.test
e "file /user/serv/lighttpd.test/config.ini exist"
t file_exist user/serv/lighttpd.test/config.ini

# file config.ini and add content
sed -i "s+^DESCRIPTION=+DESCRIPTION='test'+" user/serv/lighttpd.test/config.ini
sed -i "s+^MAINTAINER=+MAINTAINER='J.Doe'+" user/serv/lighttpd.test/config.ini
sed -i "s+^CONTENT_PATH=+CONTENT_PATH='./content'+" user/serv/lighttpd.test/config.ini
mkdir user/serv/lighttpd.test/content
echo "hello" > user/serv/lighttpd.test/content/index.html

# service start
T "service start"

e "start service lighttpd.test"
t ./midas service start lighttpd.test
e "docker container lighttpd.test up and running"
t container_running lighttpd.test

# service stop

# service list

# service explore


